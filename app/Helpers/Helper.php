<?php

namespace App\Helpers;

class Helper
{
    /**
     * Convert Array in dot notation (only object - Arr::dot())
     */
    public static function dotForm($array, $prepend = '')
    {
        if (empty($array)) {
            return $array;
        }

        $results = [];

        foreach ($array as $key => $value) {
            if (is_array($value) && key($value) !== 0 && key($value) !== null && !empty($value)) {
                $results = array_merge($results, static::dotForm($value, $prepend . $key . '.'));
            } else {
                $results[$prepend . $key] = $value;
            }
        }

        return $results;
    }

    /**
     * Function remove characters
     */
    public static function clearChar($text)
    {
        if (empty($text)) {
            return $text;
        }

        return preg_replace('/[^A-Za-z0-9\ ]/', '', $text);
    }
}
