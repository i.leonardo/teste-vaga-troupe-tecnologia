<?php

namespace App\Http\Controllers;

use App\Model\User;
use App\Helpers\Helper;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    /**
     * GET - fetch_all
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // query
        $user = User::orderBy('_nome_completo', 'DESC')->get();

        return UserResource::collection($user);
    }

    /**
     * POST - create
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        // query
        $user = User::create($request->validated());

        return new UserResource($user);
    }

    /**
     * GET - fetch:id
     *
     * @param String $id
     * @return \Illuminate\Http\Response
     */
    public function show(String $id)
    {
        // query
        $user = User::where('_id', $id)->orWhere('cpf', $id)->first();

        // no:query
        if (empty($user)) {
            return response()->json([
                'message' => 'Data not found',
            ], 404);
        }

        return new UserResource($user);
    }

    /**
     * PUT|PATCH - update:id
     *
     * @param \App\Model\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        // validate
        $validated = $request->validated();
        $input = Helper::dotForm($validated);

        // query
        $user->update($input);

        return new UserResource($user);
    }

    /**
     * DELETE - destroy:id
     *
     * @param \App\Model\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        // query
        $user->delete();

        return response()->json([
            'message' => 'deleted successfully',
        ]);
    }
}
