<?php

namespace App\Http\Requests;

use App\Helpers\Helper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class UserRequest extends FormRequest
{
    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $payload = collect([]);

        if (!empty($this->cep)) {
            $viaCEP = $this->getCep(strval($this->cep));

            $payload->put('cep', $this->cep);
            $payload->put('logradouro', empty($this->logradouro) ? Arr::get($viaCEP, 'logradouro') : $this->logradouro);
            $payload->put('bairro', empty($this->bairro) ? Arr::get($viaCEP, 'bairro') : $this->bairro);
            $payload->put('cidade', empty($this->cidade) ? Arr::get($viaCEP, 'cidade') : $this->cidade);
            $payload->put('uf', empty($this->uf) ? Arr::get($viaCEP, 'uf') : $this->uf);
        }

        if (!empty($this->cpf)) {
            $payload->put('cpf', Helper::clearChar($this->cpf));
        }

        $this->merge($payload->all());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->routeIs('users.store')) {
            // request()->isMethod('post')
            $initRule = 'required';
        } else {
            // request()->isMethod('put')
            $initRule = 'sometimes';
        }

        return [
            'nome' => "{$initRule}|string|min:3",
            'sobrenome' => "{$initRule}|string|min:3",
            'cpf' => "{$initRule}|string|unique:users",
            'email' => "{$initRule}|email",
            'celular' => "{$initRule}|string",
            'cep' => "{$initRule}|string",

            'logradouro' => "sometimes|string",
            'bairro' => "sometimes|string",
            'cidade' => "sometimes|string",
            'uf' => "sometimes|string|min:2|max:2",
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function validated($arr = null): array
    {
        $data = is_null($arr) ? parent::validated() : $arr;

        /**
         * adjusts: nome|sobrenome - ucwords
         */
        $uppercase = ['nome', 'sobrenome'];

        foreach ($uppercase as $value) {
            if (!is_null(Arr::get($data, $value))) {
                $set = ucwords(Arr::get($data, $value));

                Arr::set($data, $value, $set);
            }
        }

        /**
         * adjusts: remove char
         */
        $removeChar = ['cpf', 'celular', 'cep'];

        foreach ($removeChar as $value) {
            if (!is_null(Arr::get($data, $value))) {
                $set = Helper::clearChar(Arr::get($data, $value));

                Arr::set($data, $value, $set);
            }
        }

        // adjusts: uf - strtoupper
        if (!is_null(Arr::get($data, 'uf'))) {
            $set = strtoupper(Arr::get($data, 'uf'));

            Arr::set($data, 'uf', $set);
        }

        return $data;
    }

    /**
     * GET - ViaCEP:data
     */
    private function getCep(String $cep): array
    {
        $filtered = Helper::clearChar($cep);
        $client = new \GuzzleHttp\Client();

        try {
            $request = $client->get("https://viacep.com.br/ws/{$filtered}/json/");
            $data = json_decode($request->getBody()->getContents(), true);

            return [
                'cep' => $filtered,
                'logradouro' => Arr::get($data, 'logradouro'),
                'bairro' => Arr::get($data, 'bairro'),
                'cidade' => Arr::get($data, 'localidade'),
                'uf' => Arr::get($data, 'uf'),
            ];
        } catch (\Throwable $th) {
            return [];
        }
    }
}
