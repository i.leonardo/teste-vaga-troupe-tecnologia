<?php

namespace App\Model;

class User extends AbstractModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'sobrenome',
        'cpf',
        'email',
        'celular',
        'cep',
        'logradouro',
        'bairro',
        'cidade',
        'uf',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The accessors to append to the model's array form
     * notice that here the attribute name is in snake_case
     *
     * @var array
     */
    protected $appends = ['_nome_completo', '_regiao'];

    public function getNomeCompletoAttribute()
    {
        return "{$this->nome} {$this->sobrenome}";
    }

    public function getRegiaoAttribute()
    {
        return "{$this->cidade} - " . strtoupper($this->uf);
    }
}
