<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(User::class, function (Faker $faker) {
    $fakerBR = \Faker\Factory::create('pt_BR');

    return [
        'nome' => $fakerBR->firstName,
        'sobrenome' => $fakerBR->lastName,
        'cpf' => $fakerBR->unique()->cpf,
        'email' => $faker->unique()->safeEmail,
        'celular' => $fakerBR->cellphone,
        'cep' => $fakerBR->postcode,
        'logradouro' => $fakerBR->address,
        'bairro' => $fakerBR->streetName,
        'cidade' => $fakerBR->city,
        'uf' => $fakerBR->stateAbbr,
    ];
});
