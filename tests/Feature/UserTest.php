<?php

namespace Tests\Feature;

use App\Model\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use WithFaker;

    /** init test */
    public function setUp(): void
    {
        parent::setUp();

        // clear
        User::query()->delete();
    }

    /** @test */
    public function fetchAll()
    {
        $prefix = '[UserFetchAll]';
        $this->print("{$prefix} init");

        $this->print("- creating 3 Users...");
        factory(User::class, 3)->create();

        $this->print("- get route");
        $response = $this->getJson(route('users.index'));

        $this->print("- checking:valid");
        $response->assertStatus(200, 'expectation:statusCode: 200');
        $this->assertCount(3, $response->decodeResponseJson()['data'], 'expectation:response: count(3)');

        $this->print("{$prefix} finish");
    }

    /** @test */
    public function fetchId()
    {
        $prefix = '[UserFetchId]';
        $this->print("{$prefix} init");

        $this->print("- creating 1 User...");
        $user = factory(User::class)->create();

        $this->print("- get route");
        $response = $this->getJson(route('users.show', $user));

        $this->print("- checking:valid");
        $response->assertStatus(200, 'expectation:statusCode: 200');
        $response->assertJsonStructure([
            'data' => [
                '_id',
                'nome',
                'sobrenome',
                'cpf',
                'email',
                'celular',
                'cep',
                'logradouro',
                'bairro',
                'cidade',
                'uf',
                '_nome_completo',
                '_regiao',
                'created_at',
                'updated_at',
            ],
        ]);

        $this->print("{$prefix} finish");
    }

    /** @test */
    public function create()
    {
        $prefix = '[UserCreate]';
        $this->print("{$prefix} init");

        $this->print("- creating 1 User:form...");
        $user = factory(User::class)->make()->getAttributes();

        $this->print("- post route");
        $response = $this->postJson(route('users.store'), $user);

        $this->print("- checking:valid");
        $response->assertStatus(201, 'expectation:statusCode: 201');
        $this->assertDatabaseHas('users', [
            '_id' => $response->decodeResponseJson()['data']['_id'],
        ]);

        $this->print("{$prefix} finish");
    }

    /** @test */
    public function update()
    {
        $prefix = '[UserUpdate]';
        $this->print("{$prefix} init");

        $this->print("- creating 1 User...");
        $user = factory(User::class)->create();

        $this->print("- put route");
        $payload = ['cep' => '01001000'];
        $response = $this->putJson(route('users.update', $user), $payload);

        $this->print("- checking:valid");
        $response->assertStatus(200, 'expectation:statusCode: 200');
        $this->assertContains($payload['cep'], $response->decodeResponseJson()['data']);

        $this->print("- checking:invalid:validation");
        $payload = ['cep' => null];
        $response = $this->putJson(route('users.update', $user), $payload);
        $response->assertStatus(422, 'expectation:statusCode: 422');
        $response->assertJsonValidationErrors(['cep']);

        $this->print("{$prefix} finish");
    }

    /** @test */
    public function destroy()
    {
        $prefix = '[UserDestroy]';
        $this->print("{$prefix} init");

        $this->print("- creating 1 User...");
        $user = factory(User::class)->create();

        $this->print("- delete route");
        $response = $this->deleteJson(route('users.destroy', $user));

        $this->print("- checking:valid");
        $response->assertStatus(200, 'expectation:statusCode: 200');

        $this->print("- checking:invalid");
        $response = $this->deleteJson(route('users.destroy', $user));
        $response->assertStatus(404, 'expectation:statusCode: 404');

        $this->print("{$prefix} finish");
    }
}
